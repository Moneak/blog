<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cidades;

class CidadesController extends Controller
{
    public function consultaCidades()
    {
        $curl = curl_init();
        if (!$curl) {
            throw new RuleException(curl_error($curl));
        }

        $urlRota = "https://servicodados.ibge.gov.br/api/v1/localidades/municipios";

        $options = array(
            CURLOPT_URL => $urlRota,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_FRESH_CONNECT => 1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_CONNECTTIMEOUT => 900,
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
            ),
            );
        curl_setopt_array($curl, $options);
        $result = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        $arrCidades = json_decode($result);

        foreach ($arrCidades as $cidadeArr) {
            $cliente = null;            
            $cidade = (object) $cidadeArr;

            $objAuxCli = new \stdClass();
            $objAuxCli->nome = $cidade->nome;
            $objAuxCli->cod_ibge = $cidade->id;
            $objAuxCli->UF = $cidade->microrregiao->mesorregiao->UF->sigla;

            $cidade = new Cidades;

            $cidade->nome = $objAuxCli->nome;
            $cidade->cod_ibge = $objAuxCli->cod_ibge;
            $cidade->UF = $objAuxCli->UF;
            $cidade->save();

            
            $arrCidadeModel[] = $objAuxCli;
        }

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            print_r($arrCidadeModel);
        }
        
    }
}
