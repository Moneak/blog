<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cidades extends Model
{
    protected $table = 'cidades';

    protected $fillable = [
        "cod_ibge",
        "nome",
        "UF"
    ];

}
